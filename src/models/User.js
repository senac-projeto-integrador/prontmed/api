const { Model, DataTypes, UUID } = require('sequelize');
const { tableName } = require('./Role');

class User extends Model {
  static init(sequelize) {
    super.init({
      name: DataTypes.STRING,
      username: DataTypes.STRING,
      email: {
        type: DataTypes.STRING,
        validate: {
          isEmail:true
        }
      },
      password: DataTypes.STRING,

    }, {
      sequelize,
      tableName: "users",
      username: UUID,
      email: UUID,
      timestamps: true,
      freezeTableName: true,
      instanceMethods: {
        generateHash(password) {
          return bcrypt.hash(password, bcrypt.genSaltSync(10));
        },
        validPassword(password) {
          return bcrypt.compare(password, this.password);
        }
      }
    })
  }
  static associate(models) {
    this.belongsTo(models.Role, { foreignKey: 'role_id', as: 'role' });
  }

}

module.exports = User;

/*
freezeTableName: true,
        instanceMethods: {
            generateHash(password) {
                return bcrypt.hash(password, bcrypt.genSaltSync(8));
            },
            validPassword(password) {
                return bcrypt.compare(password, this.password);
            }
        }


async delete(req, res) {
    const { email } = req.params;
    const { name } = req.body;

    const user = await User.findByPk(user_id);

    if (!user) {
      return res.status(400).json({ 
        error: 'User not found!'
      });
    }

    const tech = await Tech.findOne({
      where: { name }
    });

    await user.removeTech(tech);

    return res.json();

  }
};


*/
