const Sequelize = require('sequelize');
const dbConfig = require('../config/database');

const Role = require('../models/Role');
const User = require('../models/User');
const MedicalRecord = require('../medical_record/models/MedicalRecord');
const Exam = require('../medical_record/models/Exam');
const ExamRequest = require('../medical_record/models/ExamRequest');
const Document = require('../medical_record/models/Document');
const Certificate = require('../medical_record/models/Certificate');
const Record = require('../medical_record/models/Record');
const Home = require('../models/Home');

const connection = new Sequelize(dbConfig);

Role.init(connection);
User.init(connection);
MedicalRecord.init(connection);
Exam.init(connection);
ExamRequest.init(connection);
Document.init(connection);
Certificate.init(connection);
Record.init(connection);
Home.init(connection);

Role.associate(connection.models);
User.associate(connection.models);
MedicalRecord.associate(connection.models);
Exam.associate(connection.models);
ExamRequest.associate(connection.models);
Document.associate(connection.models);
Certificate.associate(connection.models);
Record.associate(connection.models);
//Home.associate(connection.models);

module.exports = connection;