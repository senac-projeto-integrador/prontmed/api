const Role = require('../models/Role');
const User = require('../models/User');
const { index } = require('./RoleController');
const bcrypt = require('bcryptjs');
const { ValidationError } = require('sequelize');

module.exports = {

  async list(req, res) {
    const users = await User.findAll();

    return res.json(users);
  },

  async index(req, res) {

    const { role_id } = req.params;

    const role = await Role.findByPk(role_id, {
      include: { association: 'users' }
    });
    return res.json(role);
    // return res.json(role.users);
  },

  async store(req, res) {

    try {

      const { role_id } = req.params;
      const { name, username, email, password } = req.body;

      var salt = bcrypt.genSaltSync(10);
      var hash = bcrypt.hashSync(password, salt);

      const role = await Role.findByPk(role_id);

      if (!role) {
        return res.status(400).json({
          error: true,
          message: 'Erro: Role não encontrada!'
        });
      }

      const user = await User.create({
        name,
        username,
        email,
        password: hash,
        role_id
      });
      return res.status(200).json(user);
    } catch (err) {
      return res.status(400).json({ error: err });
    }
  },

  async delete(req, res) {
    try {

      const { username } = req.body;

      const user = await User.findOne({
        where: { username }
      });

      if (!user) {
        return res.status(400).json({
          error: true,
          message: "Usuário não encontrado!"
        })
      }

      await User.destroy({
        where: { username }
      });

      return res.status(200).json({
        error: false,
        message: "Exclusão realizada com sucesso!"
      })


    } catch (err) {
      return res.status(400).json({ error: err });
    }
  }

}