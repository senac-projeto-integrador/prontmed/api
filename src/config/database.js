module.exports = {
  dialect: 'mysql', 
  host: 'localhost',
  username: 'admin', 
  password: 'root',
  database: 'prontmed_db',
  define: {
    timestamps: true,
    timezone: "-03:00"
  }
}
