const MedicalRecord = require('../models/MedicalRecord');
const Exam = require('../models/Exam');
const { index } = require('../controllers/MedicalRecordController');
const slugify = require('slugify');

module.exports = {

  async list(req, res) {
    const exams = await Exam.findAll();

    return res.json(exams);
  },

  async index(req, res) {

    const { mr_id } = req.params;

    const medicalRecord = await MedicalRecord.findByPk(mr_id, {
      include: { association: 'exams' }
    });
    return res.json(medicalRecord);
    // return res.json(medicalRecord.exams);
  },

  async store(req, res) {

    try {

      const { mr_id } = req.params;
      const { title, body } = req.body;

      const medicalRecord = await MedicalRecord.findByPk(mr_id);

      if (!medicalRecord) {
        return res.status(400).json({
          error: true,
          message: 'Erro: Prontuário não encontrado!'
        });
      }

      const exam = await Exam.create({
        title,
        slug: slugify(title),
        body,
        mr_id
      });
      return res.status(200).json(exam);
    } catch (err) {
      return res.status(400).json({ error: err });
    }
  },

  async delete(req, res) {
    try {

      const { title } = req.body;
      const slug = slugify(title);

      const exam = await Exam.findOne({
        where: { slug }
      });

      if (!exam) {
        return res.status(400).json({
          error: true,
          message: "Exame não encontrado!"
        })
      }

      await Exam.destroy({
        where: { slug }
      });

      return res.status(200).json({
        error: false,
        message: "Exclusão realizada com sucesso!"
      })


    } catch (err) {
      return res.status(400).json({ error: err });
    }
  }

}