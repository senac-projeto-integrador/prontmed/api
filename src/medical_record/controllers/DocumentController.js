const MedicalRecord = require('../models/MedicalRecord');
const Document = require('../models/Document');
const { index } = require('../controllers/MedicalRecordController');
const slugify = require('slugify');

module.exports = {

  async list(req, res) {
    const documents = await Document.findAll();

    return res.json(documents);
  },

  async index(req, res) {

    const { mr_id } = req.params;

    const medicalRecord = await MedicalRecord.findByPk(mr_id, {
      include: { association: 'documents' }
    });
    return res.json(medicalRecord);
    // return res.json(medicalRecord.documents);
  },

  async store(req, res) {

    try {

      const { mr_id } = req.params;
      const { title, body } = req.body;

      const medicalRecord = await MedicalRecord.findByPk(mr_id);

      if (!medicalRecord) {
        return res.status(400).json({
          error: true,
          message: 'Erro: Prontuário não encontrado!'
        });
      }

      const document = await Document.create({
        title,
        slug: slugify(title),
        body,
        mr_id
      });
      return res.status(200).json(document);
    } catch (err) {
      return res.status(400).json({ error: err });
    }
  },

  async delete(req, res) {
    try {

      const { title } = req.body;
      const slug = slugify(title);

      const document = await Document.findOne({
        where: { slug }
      });

      if (!document) {
        return res.status(400).json({
          error: true,
          message: "Documento não encontrado!"
        })
      }

      await Document.destroy({
        where: { slug }
      });

      return res.status(200).json({
        error: false,
        message: "Exclusão realizada com sucesso!"
      })
    } catch (err) {
      return res.status(400).json({ error: err });
    }
  }
}