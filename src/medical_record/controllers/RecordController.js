const MedicalRecord = require('../models/MedicalRecord');
const Record = require('../models/Record');
const { index } = require('../controllers/MedicalRecordController');
const slugify = require('slugify');

module.exports = {

  async list(req, res) {
    const records = await Record.findAll();

    return res.json(records);
  },

  async index(req, res) {

    const { mr_id } = req.params;

    const medicalRecord = await MedicalRecord.findByPk(mr_id, {
      include: { association: 'records' }
    });
    return res.json(medicalRecord);
    // return res.json(medicalRecord.records);
  },

  async store(req, res) {

    try {

      const { mr_id } = req.params;
      const { title, body } = req.body;

      const medicalRecord = await MedicalRecord.findByPk(mr_id);

      if (!medicalRecord) {
        return res.status(400).json({
          error: true,
          message: 'Erro: Prontuário não encontrado!'
        });
      }

      const record = await Record.create({
        title,
        slug: slugify(title),
        body,
        mr_id
      });
      return res.status(200).json(record);
    } catch (err) {
      return res.status(400).json({ error: err });
    }
  },

  async delete(req, res) {
    try {

      const { title } = req.body;
      const slug = slugify(title);

      const record = await Record.findOne({
        where: { slug }
      });

      if (!record) {
        return res.status(400).json({
          error: true,
          message: "Exame não encontrado!"
        })
      }

      await Record.destroy({
        where: { slug }
      });

      return res.status(200).json({
        error: false,
        message: "Exclusão realizada com sucesso!"
      })
    } catch (err) {
      return res.status(400).json({ error: err });
    }
  }
}